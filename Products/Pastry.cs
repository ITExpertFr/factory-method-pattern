namespace ITExpertPastry.Products
{
    public interface Pastry 
    {
        void Bake(int temperatureInCelcius, int timeInSeconds);
    }
}