﻿using System;
using ITExpertPastry.Factories;
using ITExpertPastry.Products;

namespace ITExpertPastry
{
    class Program
    {
        static void Main(string[] args)
        {
            PastryFactory randomPastryFactory = new RandomPastryFactory();
            Pastry randomPastry = randomPastryFactory.Create();
            Pastry randomBakedPastry = randomPastryFactory.GetBakedPastry(180, 300);

            PastryFactory fromStringPastryFactory = new FromStringPastryFactory("cookie");
            Pastry pastryFromString = fromStringPastryFactory.Create();
            
            //Si on utilise la fabrique qu'une seule fois, on peut utiliser cette syntaxe
            Pastry anotherPastryFromString = new FromStringPastryFactory("donut").Create();

            Console.WriteLine(pastryFromString.GetType().Name); // Cookie
            Console.WriteLine(anotherPastryFromString.GetType().Name); // Doughnut
            Console.WriteLine(randomPastry.GetType().Name); // Impossible à prédire :/
        }
    }
}
