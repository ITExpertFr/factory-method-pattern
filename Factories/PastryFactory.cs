using System;
using ITExpertPastry.Products;

namespace ITExpertPastry.Factories
{
    public abstract class PastryFactory
    {
        public abstract Pastry Create();

        public Pastry GetBakedPastry(int temperatureInCelcius, int timeInSeconds)
        {
            Pastry result = Create();
            result.Bake(temperatureInCelcius, timeInSeconds);
            return result;
        }
    }
}