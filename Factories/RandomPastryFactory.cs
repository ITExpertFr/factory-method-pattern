using System;
using ITExpertPastry.Products;

namespace ITExpertPastry.Factories
{
    public class RandomPastryFactory : PastryFactory
    {
        public override Pastry Create()
        {
            int randomNumber = new Random().Next(1, 4);

            return randomNumber switch
            {
                1 => new Croissant(),
                2 => new Cookie(),
                _ => new Doughnut()
            };
        }
    }
}