using System;
using ITExpertPastry.Products;

namespace ITExpertPastry.Factories
{
    public class FromStringPastryFactory : PastryFactory
    {
        private string PastryName { get; set; } 

        public FromStringPastryFactory(string pastryName)
        {
            PastryName = pastryName;
        }

        public override Pastry Create()
        {
            return PastryName.ToUpper() switch
            {
                "CROISSANT" => new Croissant(),
                "COOKIE" => new Cookie(),
                "DOUGHNUT" => new Doughnut(),
                "DONUT" => new Doughnut(),
                _ => throw new InvalidOperationException()
            };
        }
    }
}