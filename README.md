# ITExpert - Factory Method
Ceci est le code source utilisé pour mon article sur les fabriques (Factory Method Pattern).

[Lien vers l'article](https://itexpert.fr/blog/factory-method-pattern)

### Prérequis
- Installer les SDK de .NET 5

### Guide d'utilisation
1. Cloner le code source.
2. Ouvrir la racine du répertoire dans une invite de commandes.
3. Éxecuter la commande "dotnet run" pour lancer le programme.

